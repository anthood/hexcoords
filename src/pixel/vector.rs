
use super::Pixel;

#[derive(Clone, Copy, Debug)]
pub struct PixelVector {
	pub x: Pixel,
	pub y: Pixel,
}

impl PixelVector {
	pub fn new(x: Pixel, y: Pixel) -> Self {
		Self { x, y }
	}
}
