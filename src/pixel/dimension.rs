
use super::*;

const SQRT_3: f32 = 1.732_050_8;
const SIN_60: f32 = 0.866_025_4;



#[derive(Clone, Copy, Debug)]
pub struct TileDimension {
	///Length of one side of the hexagon.
	hexture_edge_length: Pixel,
	///Size of the bounding rectangle.
	size: PixelVector,
	///Distance between the centers of two adjacent hextures.
	spacing: PixelVector,

	trixel_size: PixelVector,

	///The number of trixel edge lengths that fit into one hexture edge length.
	trixels_per_hexture_edge: u8,
	trixels_between_hextures: u8,
}

impl TileDimension {
	pub fn new(
		hexture_edge_length: Pixel,
		trixels_per_hexture_edge: u8,
		trixels_between_hextures: u8,
	) -> Self {

		let trixel_edge_length = hexture_edge_length / f32::from(trixels_per_hexture_edge);

		let size = PixelVector::new(hexture_edge_length * 2.0, hexture_edge_length * SQRT_3);

		let spacing = {
			let spacing_between_hexture = f32::from(trixels_between_hextures) / 2.0 * trixel_edge_length; //divide by 2, because it's added on both sides of the hexture
			let edge_length_with_spacing = hexture_edge_length + spacing_between_hexture;

			PixelVector::new(edge_length_with_spacing * 3.0/2.0, edge_length_with_spacing * SQRT_3)
		};

		let trixel_size = PixelVector::new(
			trixel_edge_length,
			trixel_edge_length * SIN_60,
		);

		Self {
			hexture_edge_length,
			size,
			spacing,
			trixel_size,
			trixels_per_hexture_edge,
			trixels_between_hextures,
		}
	}

	pub fn change_edge_length(&mut self, factor: f32) {
		*self = Self::new(
			self.hexture_edge_length * factor,
			self.trixels_per_hexture_edge,
			self.trixels_between_hextures
		);
	}

	pub fn hexture_edge_length(&self) -> Pixel {
		self.hexture_edge_length
	}

	pub fn size(&self) -> &PixelVector {
		&self.size
	}

	pub fn spacing(&self) -> &PixelVector {
		&self.spacing
	}

	pub fn trixel_size(&self) -> PixelVector {
		self.trixel_size
	}
}
