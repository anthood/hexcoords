
type Pixel = f32;

mod vector;
pub use vector::PixelVector;

mod dimension;
pub use dimension::TileDimension;
