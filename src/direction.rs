use super::PositionVector;


#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Direction {
	NorthWest,
	North,
	NorthEast,
	SouthWest,
	South,
	SouthEast,
}

use Direction::*;
impl Direction {
	///Returns an array of the variants that this enum has.
	pub fn variants() -> [Direction; 6] {
		[
			North,
			NorthEast,
			SouthEast,
			South,
			SouthWest,
			NorthWest,
		]
	}
	
	///Returns the radian angle associated with this direction.
	pub fn angle_radian(&self) -> f32 {
		use std::f32::consts::{FRAC_PI_3, PI};
		match self {
			North     => 0.0,
			NorthEast => FRAC_PI_3,
			SouthEast => FRAC_PI_3 * 2.0,
			South     => PI,
			SouthWest => FRAC_PI_3 * 4.0,
			NorthWest => FRAC_PI_3 * 5.0,
		}
	}
	
	///Returns the neighboring direction going clock-wise.
	///```
	/// # use hexcoords::Direction::*;
	///assert_eq!(North.neighbor_cw(), NorthEast);
	///assert_eq!(SouthWest.neighbor_cw(), NorthWest);
	///```
	pub fn neighbor_cw(&self) -> Direction {
		match self {
			North     => NorthEast,
			NorthEast => SouthEast,
			SouthEast => South,
			South     => SouthWest,
			SouthWest => NorthWest,
			NorthWest => North,
		}
	}
	
	///Returns the neighboring direction going counter-clock-wise.
	///```
	/// # use hexcoords::Direction::*;
	///assert_eq!(North.neighbor_ccw(), NorthWest);
	///assert_eq!(SouthEast.neighbor_ccw(), NorthEast);
	///```
	pub fn neighbor_ccw(&self) -> Direction {
		match self {
			North     => NorthWest,
			NorthEast => North,
			SouthEast => NorthEast,
			South     => SouthEast,
			SouthWest => South,
			NorthWest => SouthWest,
		}
	}
	
	///Returns the direction on the opposite side.
	///```
	/// # use hexcoords::Direction::*;
	///assert_eq!(North.opposite(), South);
	///assert_eq!(SouthEast.opposite(), NorthWest);
	pub fn opposite(&self) -> Direction {
		match self {
			North     => South,
			NorthEast => SouthWest,
			SouthEast => NorthWest,
			South     => North,
			SouthWest => NorthEast,
			NorthWest => SouthEast,
		}
	}
	
	///Returns a normalized vector pointing into the given direction.
	///Equivalent to one step into that direction.
	pub fn normalized_vector(&self) -> PositionVector {
		match self {
			North     => PositionVector::new( 0, -1),
			NorthEast => PositionVector::new( 1, -1),
			SouthEast => PositionVector::new( 1,  0),
			South     => PositionVector::new( 0,  1),
			SouthWest => PositionVector::new(-1,  1),
			NorthWest => PositionVector::new(-1,  0),
		}
	}
}


impl std::fmt::Display for Direction {
	fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
		write!(formatter, "{}",
			match self {
				North     => "North",
				NorthEast => "North-East",
				SouthEast => "South-East",
				South     => "South",
				SouthWest => "South-West",
				NorthWest => "North-West",
			}
		)
	}
}
