use crate::{Position, Direction};
use super::{HexagonalSpiralIterator, HexagonalArea};


pub struct HexagonalAreaSpiralIterator {
	center: Option<Position>,
	spiral_iterators: [HexagonalSpiralIterator; 6],
	current_spiral_iterator: usize,
}

impl HexagonalAreaSpiralIterator {
	pub fn new(hexagonal_area: &HexagonalArea) -> Self {
		
		let spiral_iterators = [
			HexagonalSpiralIterator::new(hexagonal_area, Direction::North),
			HexagonalSpiralIterator::new(hexagonal_area, Direction::NorthEast),
			HexagonalSpiralIterator::new(hexagonal_area, Direction::SouthEast),
			HexagonalSpiralIterator::new(hexagonal_area, Direction::South),
			HexagonalSpiralIterator::new(hexagonal_area, Direction::SouthWest),
			HexagonalSpiralIterator::new(hexagonal_area, Direction::NorthWest),
		];
		
		Self {
			center: Some(hexagonal_area.center),
			spiral_iterators,
			current_spiral_iterator: 0,
		}
	}
}

impl std::iter::Iterator for HexagonalAreaSpiralIterator {
	type Item = Position;
	
	
	fn next(&mut self) -> Option<Self::Item> {
		
		if let Some(center) = self.center { //first iteration step returns the center
			self.center = None;
			return Some(center);
		}
		
		
		let spiral_iterator = self.spiral_iterators.get_mut(self.current_spiral_iterator).expect("Failed to retrieve SpiralIterator.");
		self.current_spiral_iterator = (self.current_spiral_iterator + 1) % 6;
		
		
		spiral_iterator.next()
	}
}
