use crate::Position;
use super::HexagonalAreaSpiralIterator;

#[derive(Clone)]
pub struct HexagonalArea {
	pub(super) center: Position,
	pub(super) radius: u8,
}

impl HexagonalArea {
	pub fn new(center: Position, radius: u8) -> Self {
		Self {
			center,
			radius,
		}
	}
	
	pub fn spiral_iter(&self) -> HexagonalAreaSpiralIterator {
		HexagonalAreaSpiralIterator::new(self)
	}
}



