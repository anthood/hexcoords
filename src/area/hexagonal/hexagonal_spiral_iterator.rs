use crate::{Position, Direction};
use super::HexagonalArea;


pub struct HexagonalSpiralIterator {
	radius: u8,
	ring: u8,
	step_in_ring: u8,
	direction: Direction,
	position: Position,
}

impl HexagonalSpiralIterator {
	pub fn new(hexagonal_area: &HexagonalArea, starting_direction: Direction) -> Self {
		let starting_position = hexagonal_area.center + starting_direction.normalized_vector();
		
		Self {
			radius: hexagonal_area.radius,
			ring: 0,
			step_in_ring: 0,
			direction: starting_direction,
			position: starting_position,
		}
	}
}

impl std::iter::Iterator for HexagonalSpiralIterator {
	type Item = Position;
	
	fn next(&mut self) -> Option<Self::Item> {
		
		self.step_in_ring += 1;
		
		if self.step_in_ring <= self.ring && self.step_in_ring < self.radius {
			self.position += self.direction.normalized_vector();
		}
		else {
			self.ring += 1;
			
			if self.ring <= self.radius {
				self.step_in_ring = 0;
				self.direction = self.direction.neighbor_cw();
			}
			else {
				return None;
			}
		}
		
		Some(self.position)
	}
}
