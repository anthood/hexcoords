use super::{HexCoord, Position};


#[derive(Debug, Clone, Copy, PartialEq)]
pub struct PositionVector {
	pub col: HexCoord,
	pub row: HexCoord,
}

impl PositionVector {
	pub fn new(col: HexCoord, row: HexCoord) -> Self {
		Self {
			col,
			row,
		}
	}
	
	
	pub fn to_position(&self) -> Position {
		Position { col: self.col, row: self.row }
	}
}
