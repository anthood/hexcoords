use super::{Position, PositionVector};


#[derive(Debug, Clone, PartialEq)]
pub struct Distance {
	pub start: Position,
	pub end: Position,
}

impl Distance {
	pub fn new(start: Position, end: Position) -> Self {
		Self { start, end }
	}


	pub fn to_vector(&self) -> PositionVector {
		self.end - self.start
	}


	pub fn distance_b_line(&self) -> u8 {
		let col_distance = (self.start.col - self.end.col).abs();
		let row1_distance = (self.start.row - self.end.row).abs();
		let row2_distance = (self.start.row2() - self.end.row2()).abs();

		col_distance.max(row1_distance).max(row2_distance) as u8
	}
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn to_vector() {
		let pos1 = Position::new(-1,  1);
		let pos2 = Position::new( 1, -1);

		assert_eq!(pos1.to(pos2).to_vector(), PositionVector::new(2, -2));
	}


	#[test]
	fn distance_b_line() {
		let center = Position::new( 0,  0);

		assert_eq!(center.to(Position::new( 0,  1)).distance_b_line(), 1);
		assert_eq!(center.to(Position::new(-1,  0)).distance_b_line(), 1);
		assert_eq!(center.to(Position::new( 1,  1)).distance_b_line(), 2);
		assert_eq!(center.to(Position::new(-1,  1)).distance_b_line(), 1);
		assert_eq!(center.to(Position::new( 1, -1)).distance_b_line(), 1);
		assert_eq!(center.to(Position::new(-1, -1)).distance_b_line(), 2);
	}
}
