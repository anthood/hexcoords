use super::{HexCoord, PositionVector, Distance};


#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Position {
	pub col: HexCoord,
	pub row: HexCoord,
}


impl Position {
	pub fn new(col: HexCoord, row: HexCoord) -> Self {
		Self { col, row }
	}
	
	pub fn row2(&self) -> HexCoord {
		-self.col - self.row
	}
	
	pub fn to(&self, other: Position) -> Distance {
		Distance::new(*self, other)
	}
}



impl std::ops::Add<PositionVector> for Position {
	type Output = Self;
	
	fn add(self, relative: PositionVector) -> Self {
		Self {
			col: self.col + relative.col,
			row: self.row + relative.row,
		}
	}
}

impl std::ops::AddAssign<PositionVector> for Position {
	fn add_assign(&mut self, other: PositionVector) {
		*self = *self + other
	}
}



impl std::ops::Sub<PositionVector> for Position {
	type Output = Self;
	
	fn sub(self, relative: PositionVector) -> Self {
		Self {
			col: self.col - relative.col,
			row: self.row - relative.row,
		}
	}
}



impl std::ops::Sub<Position> for Position {
	type Output = PositionVector;
	
	fn sub(self, other: Position) -> PositionVector {
		PositionVector {
			col: self.col - other.col,
			row: self.row - other.row,
		}
	}
}



#[cfg(test)]
mod tests {
	use super::*;
	
	#[test]
	fn vector_add() {
		let position = Position::new(1, -1);
		let vector = PositionVector::new(2, 2);
		
		let expectation = Position::new(3, 1);
		
		assert_eq!(position + vector, expectation);
	}
	
	#[test]
	fn vector_sub() {
		let position = Position::new(1, -1);
		let vector = PositionVector::new(2, 2);
		
		let expectation = Position::new(-1, -3);
		
		assert_eq!(position - vector, expectation);
	}
	
	#[test]
	fn position_sub() {
		let position1 = Position::new(1, -1);
		let position2 = Position::new(2,  2);
		
		let expectation = PositionVector::new(-1, -3);
		
		assert_eq!(position1 - position2, expectation);
	}
}
