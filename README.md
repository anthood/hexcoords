# HexCoords

Yet another Rust library for handling 2D coordinates on hexagonal grids.

Built for use in [Anthood](https://codeberg.org/anthood/anthood), so may not support all use-cases.  
In particular, it is only built for flat hexagonal grids.

Lots of information has been taken from [https://www.redblobgames.com/grids/hexagons/](https://www.redblobgames.com/grids/hexagons/).
